# Custom JSS Icons

## Function
Allows easy customization of the branding within a Jamf Pro web interface.

## Tested Environment(s)
RHEL 7 & CentOS 7

## Changelog

**v1.0 (2015-10-29)**
* Original Release
